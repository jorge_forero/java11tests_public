package com.jesoftware;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class Java11FeaturesTest {

    private Java11Features java11Features;

    @BeforeEach
    void setUp() {
        java11Features = new Java11Features();
    }

    @Test
    void splitLinesTest() {
        String multilineString = "Baeldung helps \n \n developers \n explore Java.";
        List<String> lines = java11Features.separarLineas(multilineString);
        assertThat(lines).containsExactly("Baeldung helps", "developers", "explore Java.");
    }

    @Test void repeatStringTest(){
        String test = "Java ";
        String newTest = test.repeat(2);
        assertThat(newTest).isEqualTo("Java Java ");
    }



    @Test
    void writeStringFile() {
        String testText = "Hello World!";
        String readText = java11Features.writeStringFile(testText);
        assertThat(readText).isEqualTo(testText);
    }

    @Test
    void toArray() {
        List<String> sampleList = Arrays.asList("Java", "Kotlin");
        String[] sampleArray = java11Features.toArray(sampleList, String.class);
        assertThat(sampleArray).containsExactly("Java", "Kotlin");
    }

    @Test
    void toArray2() {
        List<Integer> sampleList = Arrays.asList(1, 4);
        Integer[] sampleArray = java11Features.toArray(sampleList, Integer.class);
        assertThat(sampleArray).containsExactly(1, 4);
    }

    @Test
    void toArray3() {
        List<Integer> sampleList = new ArrayList<>();
        Integer[] sampleArray = java11Features.toArray(sampleList, Integer.class);
        assertThat(sampleArray).isEmpty();
    }

    @Test
    void notPredicate() {
        String multilineString = "Baeldung helps \n \n developers \n explore Java.";
        List<String> lines = java11Features.notPredicate(multilineString);
        assertThat(lines).containsExactly("Baeldung helps", "developers", "explore Java.");
    }

    @Test
    void varInLambdaParameters() {
        List<String> words = Arrays.asList("Java", "Kotlin");
        String result = java11Features.varInLambdaParameters(words);
        assertThat(result).isEqualTo("JAVA, KOTLIN");
    }

    @Test
    void toStringArrayTest() {
        List<String> testList = Arrays.asList("Java", "Kotlin");
        String[] stringArray = java11Features.toStringArray(testList);
        assertThat(stringArray).containsExactly("Java", "Kotlin");
    }

    @Test
    void toIntegerArrayTest() {
        List<Integer> testList = Arrays.asList(3, 4);
        Integer[] integerArray = java11Features.toIntegerArray(testList);
        assertThat(integerArray).containsExactly(3, 4);
    }
}