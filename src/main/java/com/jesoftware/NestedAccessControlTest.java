package com.jesoftware;


import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class NestedAccessControlTest {

    public static void main(String[] args) {

        Outter outerObject = new Outter();
        Outter.Nested nested = outerObject.new Nested();
        nested.nestedPublic();

        assertThat(Outter.class.isNestmateOf(Outter.Nested.class)).isTrue();

        Set<String> nestedMembers = Arrays.stream(Outter.Nested.class.getNestMembers())
                .map(Class::getName)
                .collect(Collectors.toSet());
        assertThat(nestedMembers).contains(Outter.class.getName(), Outter.Nested.class.getName());

    }
}

class Outter {

    public void myPublic() {
    }

    private void myPrivate() {
        System.out.println("Hello from private Outter.myPrivate()!");
    }

    class Nested {

        public void nestedPublic() {
            myPrivate();
        }
    }
}
