package com.jesoftware;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Java11Features {

    public List<String> separarLineas(String multilineString){
        List<String> lines = multilineString.lines() //stream of lines
                .filter(line -> !line.isBlank())
                .map(String::strip)
                .collect(Collectors.toList());
        return lines;
    }

    public String writeStringFile(String text){
        String fileContent = null;
        try{
            Path filePath = Files.writeString(Files.createTempFile( //writes a String to a file
                    Path.of(System.getProperty("user.dir")), "tempFile", ".txt"), text);
            System.out.println(filePath.toAbsolutePath().toString());
            fileContent = Files.readString(filePath);
        }catch(IOException ioe){
            System.out.println(ioe.getMessage());
        }
        return fileContent;
    }

    public String[] toStringArray(List<String> testList){
        return testList.toArray(String[]::new);
    }

    public Integer[] toIntegerArray(List<Integer> testList){
        return testList.toArray(Integer[]::new);
    }

    public <T> T[] toArray(List<T> list, Class<T> tClass){
        System.out.println(list);

        //Creates an array of the required type, according to the list elements' type
        T[] array = (T[])Array.newInstance(tClass, list.size());

        return list.toArray(array); //Converts a generic list to the respective array type
    }

    public List<String> notPredicate(String multilineString){
        return multilineString.lines()
                .filter(Predicate.not(String::isBlank)) //negation of the supplied predicate
                .map(String::strip)
                .collect(Collectors.toList());
    }

    public String varInLambdaParameters(List<String> words){
        return words.stream()
                .map((var x) -> x.toUpperCase()) //use of var in lambda parameters
                .collect(Collectors.joining(", "));
    }


    }
